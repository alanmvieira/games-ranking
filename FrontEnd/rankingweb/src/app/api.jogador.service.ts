import { Http, Headers, Response} from '@angular/http';
import { Injectable } from '@angular/core';
import { Jogador } from "./shared/jogador.model";
import {Observable} from 'rxjs/observable';

import 'rxjs/add/operator/map'

import 'rxjs/add/operator/toPromise';

@Injectable()
export class JogadorService{
    
   constructor(private http: Http){
    }
    
    public addJogador(jogador: Jogador): Promise<any>{
        
        let headers = new Headers();
        headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append("Content-type","application/json");

        return this.http.post('http://localhost:8080/api/jogador', 
            JSON.stringify(jogador), {headers: headers}).
            toPromise()
            .then( (resposta:any) => resposta)
            .catch((e) => {

                let mensagemErro;
                if(e.status === 400){
                    mensagemErro = "Nome do jogador não informado.";
                }
                if(e.status === 406){
                    mensagemErro = "Jogador já cadastrado.";
                }
                let retorno = {
                    "mensagem" : "erro",
                    "mensagemErro": mensagemErro
                }
                
                return retorno});
    }

    public getJogadores(): Promise<any[]>{

        let headers = new Headers();
        headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type');
        headers.append('Access-Control-Allow-Methods', 'GET, OPTIONS');
        headers.append('Access-Control-Allow-Origin', '*');

        return this.http.get('http://localhost:8080/api/jogadores', {headers: headers})
            .toPromise()
            .then((resposta:any) => resposta.json())
            .catch((e) => {return "erroGetJogadores"});
    }

    public addPartida(nomeJogador: string): Promise<any>{
        
        let headers = new Headers();
        headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
        headers.append('Access-Control-Allow-Methods', 'Put');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append("Content-type","application/json");

        let jogador = {
            "nome": nomeJogador
        }
        return this.http.put('http://localhost:8080/api/jogador/partida', 
            JSON.stringify(jogador), {headers: headers}).
            toPromise()
            .then((resposta:any) => resposta)
            .catch((resposta:any) => resposta);
        
    }

    public addVitoria(nomeJogador: string): Promise<any>{
        
        let headers = new Headers();
        headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
        headers.append('Access-Control-Allow-Methods', 'Put');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append("Content-type","application/json");

        let jogador = {
            "nome": nomeJogador
        }
        return this.http.put('http://localhost:8080/api/jogador/vitoria', 
            JSON.stringify(jogador), {headers: headers}).
            toPromise()
            .then( (resposta:any) => resposta)
            .catch((resposta:any) => resposta);
        
    }

}
