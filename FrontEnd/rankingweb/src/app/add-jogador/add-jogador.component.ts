import { Component, OnInit, EventEmitter } from '@angular/core';
import {JogadorService} from '../api.jogador.service';

import { EventEmitterService } from "../shared/eventemitter.service";
import { Jogador } from '../shared/jogador.model';

@Component({
  selector: 'app-add-jogador',
  templateUrl: './add-jogador.component.html',
  providers: [ JogadorService ]
})
export class AddJogadorComponent implements OnInit {

  public nomeJogador: string;
  public partidas: number;
  public vitorias: number;
  static jogadorCriado = new EventEmitter<string>();
  public erroMensagem: string;
  
  constructor(private jogadorService : JogadorService) { }

  ngOnInit() {
  }

  public setNomeJogador(input: Event): void{
    this.nomeJogador = (<HTMLInputElement> input.target).value;
  }

  public setPartidas(input: Event): void{
    this.partidas = parseInt((<HTMLInputElement> input.target).value);
  }

  public setVitorias(input: Event): void{
    this.vitorias = parseInt((<HTMLInputElement> input.target).value);
  }

  public addJogador(): void{
    
    this.erroMensagem = undefined;

    let jogador = {
      "nome": this.nomeJogador,
      "partidas": this.partidas,
      "vitorias": this.vitorias,
    }

    this.jogadorService.addJogador(jogador).
      then((resposta: any) => {
        
        if (resposta.mensagem === "erro"){
          this.erroMensagem = resposta.mensagemErro; 
        }else {
          AddJogadorComponent.jogadorCriado.emit("atualizarJogadores");
        }

      });
      
  }

}
