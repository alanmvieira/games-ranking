import { Component, OnInit } from '@angular/core';
import {JogadorService} from '../api.jogador.service';
import { Jogador } from '../shared/jogador.model';

import { EventEmitterService } from "../shared/eventemitter.service";
import { AddJogadorComponent } from '../add-jogador/add-jogador.component';

@Component({
  selector: 'app-listagem-jogador',
  templateUrl: './listagem-jogador.component.html',
  providers: [ JogadorService ]
})
export class ListagemJogadorComponent implements OnInit {

  public jogadores : Jogador[];

  constructor(private jogadorService : JogadorService) {
  }

  ngOnInit() {
    
    this.jogadorService.getJogadores()
      .then((retorno: any ) => {
        
        if (retorno === "erroGetJogadores"){
        }else {
          if(retorno === null){
            this.jogadores = null;
          }else {
            this.jogadores = retorno;
          }

        }
      });
    
    AddJogadorComponent.jogadorCriado.subscribe(retorno => this.getJogadores());
  
  }

  public getJogadores(): void{

    this.jogadorService.getJogadores()
      .then((jogadores: Jogador[] ) => {
        this.jogadores = jogadores;
      });
  }


  public addPartida(jogador: Jogador): void{
    
    this.jogadorService.addPartida(jogador.nome)
      .then((resposta: any ) => {
        if(resposta.status == 200){
          this.getJogadores();
        }
      })

  }

  public addVitoria(jogador: Jogador): void{
    this.jogadorService.addVitoria(jogador.nome)
      .then((resposta: any ) => {
        if(resposta.status == 200){
          this.getJogadores();
        }
      })
  }

}
