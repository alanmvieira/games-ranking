# FrontEnd - Angular

Projeto desenvolvimento para ser a parte Front-End do desafio. O projeto foi desenvolvido com o nome de  **rankingweb**.

# Pré-requisitos para execução

Para fazer a execução do projeto é necessário que na máquina tenha instalado o "npm". 

# Execução

Após fazer o checkout do projeto, executar em um terminal o comando **npm install** para baixar as dependências necessárias.
Para a execução, basta executar o comando **ng server**. Após a execução do comando é possível acessado o projeto através da URL: "http://localhost:4200/" 

# Tecnologias Utilizadas

Foi utilizado Angular 5 para o desenvolvimento. 

# Melhorias
