# BackEnd - API REST

Projeto desenvolvimento para ser a parte backend (api rest) do desafio. O projeto foi desenvolvido com o nome de  **gameranking**.

# Pré-requisitos para execução

Para fazer a execução do projeto é necessário que na máquina tenha instalado Java 8 e maven. 

# Execução

Após fazer o checkout do projeto, executar em um terminal o comando **mvn install** para baixar as dependências necessárias. O próximo comando será **mvn package**, esse comando fará a compilação do código desenvolvido e execução dos testes. Após a execução do comando citado um arquivo com o nome **gameranking-0.0.1-SNAPSHOT** será gerado dentro da pasta "target". Para a execução do arquivo basta executar **java -jar gameranking-0.0.1-SNAPSHOT**.
Ao executar o comando será inicializado um servidor que disponibilizará os serviços criados com sprint boot e um banco de dados.

## Serviços REST Disponibilizados
Com o servidor em execução alguns serviços REST são disponibilizados:

- Adição de jogador
-- URI: http://localhost:8080/api/jogador -- Método: POST
-- Parâmetros enviados: JSON com a seguinte estrutura
 {
	 "nome": 'nome do jogador para cadastrar',
	 "vitorias": quantidade de vitorias, 
	 "partidas": quantidade de partidas
    }
-- Função: Adicionar um novo jogador no banco de dados
-- Retorno: Caso a execução aconteça com sucesso será retornado o status 'Created' (código 201) do http status. Caso tenha algum problema na execução será retornado o status 'Bad Request' (código 400).
-- Observação: os campos 'vitorias' e 'partidas' são opcionais no json de envio.

- Aumento do número de vitória
-- URI: http://localhost:8080/api/jogador/vitoria
-- Método: PUT
-- Parâmetros enviados: JSON com a seguinte estrutura
 {
	 "nome": 'nome do jogador que terá a quantidade de vitórias alterada'
    }
-- Função: Acrescentar o número de vitórias do jogador
-- Retorno: Caso a execução aconteça com sucesso será retornado o status 'OK' (código 200) do http status. Caso tenha algum problema na execução será retornado o status 'No Content' (código 204).

- Aumento do número de partida
-- URI: http://localhost:8080/api/jogador/partida
-- Método: PUT
-- Parâmetros enviados: JSON com a seguinte estrutura
 {
	 "nome": 'nome do jogador que terá a quantidade de partidas alterada'
    }
-- Função: Acrescentar o número de partidas do jogador
-- Retorno: Caso a execução aconteça com sucesso será retornado o status 'OK' (código 200) do http status. Caso tenha algum problema na execução será retornado o status 'No Content' (código 204)

- Listagem do jogadores cadastrados
-- URI: http://localhost:8080/api/jogadores
-- Método: GET
-- Parâmetros enviados: nenhum parâmetro é necessário.
-- Função: Retornar a lista de todos os jogadores cadastrados. 
-- Retorno: Caso a execução aconteça com sucesso será retornado o status 'OK' (código 200) do http status junto com a lista de todos os jogadores. Caso tenha algum problema na execução ou não exista nenhum jogador cadastrado será retornado o status 'No Content' (código 204).

## Banco de dados

Foi utilizado o banco de dados H2 para armazenar as informações dos jogadores. 
O banco de dados pode ser acessado via browser através da URL "http://localhost:8080/ranking/". Para acesso os dados de usuário e senha são: cast.

# Tecnologias Utilizadas

Foi utilizado as tecnologias informadas como pré-requisitos: Java 8, sprint boot, Mockito e Cucumber. 

# Testes

Os arquivos de testes podem ser encontrados nos seguintes pacotes: "test.com.cast.service" e "test.cucumber.com.cast.service"

# Melhorias

- Substituir a utilização de JDBC para mapeamento relacional
- Aumentar o número de testes

