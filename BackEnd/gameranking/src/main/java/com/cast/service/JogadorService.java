package com.cast.service;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.cast.model.Jogador;
import com.cast.repository.JogadorRepository;

public class JogadorService {

	private JogadorRepository jogadorRepository = new JogadorRepository();

	private boolean existeJogador(Jogador jogador) {
		return jogadorRepository.existeJogadorCadastro(jogador.getNome());
	}

	public HttpStatus addJogador(Jogador jogador) {

		if (jogador.getNome().equals("") ) {
			return HttpStatus.BAD_REQUEST;
		}
		
		if (existeJogador(jogador)) {
			return HttpStatus.NOT_ACCEPTABLE;
		}
		
		jogadorRepository.addJogador(jogador);

		return HttpStatus.CREATED;
	}

	public List<Jogador> getJogadores() {
		return jogadorRepository.getJogadores();
	}

	public boolean addVitoria(Jogador jogador) {

		if (jogador.getNome().equals("") || !existeJogador(jogador)) {
			return false;
		}

		jogadorRepository.addVitoria(jogador);

		return true;
	}

	public boolean addPartida(Jogador jogador) {

		if (jogador.getNome().equals("") || !existeJogador(jogador)) {
			return false;
		}

		jogadorRepository.addPartida(jogador);

		return true;
	}
	
	@SuppressWarnings("unused")
	private Jogador getJogador(Jogador jogador) {

		if (jogador.getNome().equals("")) {
			return new Jogador();
		}else {
			return jogadorRepository.getJogador(jogador.getNome());
		}

	}
}
