package com.cast.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.cast.model.Jogador;

@Repository
public class JogadorRepository {

	private static final String JDBC_DRIVER = "org.h2.Driver";
	private static final String DB_URL = "jdbc:h2:~/ranking;DB_CLOSE_ON_EXIT=FALSE";

	private static final String USER = "cast";
	private static final String PASS = "cast";

	private static final String SQL_INSERT = "INSERT INTO jogador (nome, vitorias, partidas) values (?,?,?)";
	private static final String SQL_SELECT = "SELECT * FROM jogador ORDER BY vitorias desc";
	private static final String SQL_UPDATE_VITORIAS = "UPDATE jogador SET vitorias = vitorias + 1 WHERE nome = ?";
	private static final String SQL_UPDATE_PARTIDAS = "UPDATE jogador SET partidas = partidas+ 1 WHERE nome = ?";

	JdbcTemplate jdbcTemplate;

	{
		String driverClassName = JDBC_DRIVER;
		String url = DB_URL;
		String dbUsername = USER;
		String dbPassword = PASS;

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(url);
		dataSource.setUsername(dbUsername);
		dataSource.setPassword(dbPassword);

		jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);

	}

	public void addJogador(Jogador jogador) {
		try {

			String InsertQuery = SQL_INSERT;
			jdbcTemplate.update(InsertQuery, jogador.getNome(), jogador.getVitorias(), jogador.getPartidas());

		} catch (Exception e) {

		}
	}

	class JogadorRowMapper implements RowMapper<Jogador> {
		@Override
		public Jogador mapRow(ResultSet rs, int rowNum) throws SQLException {

			String nome = rs.getString("nome");
			int vitorias = rs.getInt("vitorias");
			int partidas = rs.getInt("partidas");

			Jogador jogador = new Jogador(nome, vitorias, partidas);
			return jogador;
		}
	}

	public List<Jogador> getJogadores() {
		return jdbcTemplate.query(SQL_SELECT, new JogadorRowMapper());
	}

	public boolean existeJogadorCadastro(String nome) {

		boolean existe = false;

		Connection conn = null;
		PreparedStatement statement = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			String sql = "Select * from Jogador where nome = ?";

			statement = conn.prepareStatement(sql);
			statement.setString(1, nome);

			ResultSet rs = statement.executeQuery();

			existe = rs.first();

			rs.close();
			statement.close();
			conn.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException se2) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		return existe;

	}

	public void addVitoria(Jogador jogador) {

		try {
			jdbcTemplate.update(SQL_UPDATE_VITORIAS, jogador.getNome());
		} catch (Exception e) {
		}

	}

	public void addPartida(Jogador jogador) {

		try {
			jdbcTemplate.update(SQL_UPDATE_PARTIDAS, jogador.getNome());
		} catch (Exception e) {
		}

	}

	public Jogador getJogador(String nome) {

		List<Jogador> jogadores = new ArrayList<Jogador>();

		Connection conn = null;
		PreparedStatement statement = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			String sql = "Select * from Jogador where nome = ?";

			statement = conn.prepareStatement(sql);
			statement.setString(1, nome);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {

				String nomeJogador = rs.getString("nome");
				int vitorias = rs.getInt("vitorias");
				int partidas = rs.getInt("partidas");

				Jogador jogador = new Jogador(nomeJogador, vitorias, partidas);
				jogadores.add(jogador);

			}

			if (jogadores.size() == 1) {
				return jogadores.get(0);
			}

			rs.close();
			statement.close();
			conn.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException se2) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		return new Jogador();

	}

}
