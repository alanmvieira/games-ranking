package com.cast.configuracao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.cast.api", "com.cast.repository" })
public class Server {

	public static void main(String[] args) {
		SpringApplication.run(Server.class, args);
	}

}