package com.cast.api;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.cast.model.Jogador;
import com.cast.service.JogadorService;

@RestController
@RequestMapping("/api")
public class RankingController {

	private JogadorService jogadorService = new JogadorService();

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/jogador", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody Jogador jogador, UriComponentsBuilder ucBuilder) {

		HttpHeaders headers = new HttpHeaders();

		if (jogador.getNome() == null) {
			return new ResponseEntity<String>(headers, HttpStatus.BAD_REQUEST);
		}

		HttpStatus httpstatus = jogadorService.addJogador(jogador);

		if (httpstatus == HttpStatus.CREATED) {
			return new ResponseEntity<String>(headers, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<String>(headers, httpstatus);
		}

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/jogadores", method = RequestMethod.GET)
	public ResponseEntity<List<Jogador>> getJogadores() {

		List<Jogador> jogadores = jogadorService.getJogadores();

		if (jogadores.isEmpty()) {
			return new ResponseEntity<List<Jogador>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Jogador>>(jogadores, HttpStatus.OK);

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/jogador/vitoria", method = RequestMethod.PUT)
	public ResponseEntity<?> addVitoria(@RequestBody Jogador jogador) {

		HttpHeaders headers = new HttpHeaders();

		if (jogadorService.addVitoria(jogador)) {
			return new ResponseEntity<String>(headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(headers, HttpStatus.NO_CONTENT);
		}

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/jogador/partida", method = RequestMethod.PUT)
	public ResponseEntity<?> addPartida(@RequestBody Jogador jogador) {

		HttpHeaders headers = new HttpHeaders();

		if (jogadorService.addPartida(jogador)) {
			return new ResponseEntity<String>(headers, HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(headers, HttpStatus.NO_CONTENT);
		}
	}

}
