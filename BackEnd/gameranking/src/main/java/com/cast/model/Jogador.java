package com.cast.model;

public class Jogador {

	private String nome;
	private int vitorias = 0;
	private int partidas = 0;

	public Jogador() {

	}

	public Jogador(String nome, int vitorias, int partidas) {
		super();
		this.nome = nome;
		this.vitorias = vitorias;
		this.partidas = partidas;
	}

	public String getNome() {
		return nome;
	}

	public int getVitorias() {
		return vitorias;
	}

	public int getPartidas() {
		return partidas;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setVitorias(int vitorias) {
		this.vitorias = vitorias;
	}

	public void setPartidas(int partidas) {
		this.partidas = partidas;
	}

	public void addVitoria() {
		this.vitorias += 1;
	}

	public void addPartida() {
		this.partidas += 1;
	}

	@Override
	public String toString() {
		return "Jogador [nome=" + nome + "]";
	}

}
