package test.cucumber.com.cast.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.cast.model.Jogador;
import com.cast.service.JogadorService;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class JogadorServiceTestCucumber {

	private String nomeJogador = "";
	private List<Jogador> jogadoresRetorno = null;
	
	private JogadorService jogadorService = new JogadorService();
	private JogadorService jogadorServiceMocked = null;

	@Given("^O nome do jogador for vazio '(.*)'$")
	public void nomeJogadorVazio(String nomeJogadorInformador) {
		nomeJogador = nomeJogadorInformador;
	}

	@Then("^O resultado será falso para criar o jogador")
	public void assertSingleResult() {
		Jogador jogador = new Jogador(nomeJogador, 0, 0);
		assertEquals(HttpStatus.BAD_REQUEST,jogadorService.addJogador(jogador));
	}
	
	@Given("^quando for chamado o método de buscar todos os jogadores")
	public void buscarTodosJogadores() {
		
		MockitoAnnotations.initMocks(this);
		
		jogadorServiceMocked = Mockito.mock(JogadorService.class);		
		Jogador jogador1 = new Jogador("Jogador 1", 0, 0);
		Jogador jogador2 = new Jogador("Jogador 2", 0, 0);
		List<Jogador> jogadores = new ArrayList<>(Arrays.asList(jogador1, jogador2));

		Mockito.doReturn(jogadores).when(jogadorServiceMocked).getJogadores();

		jogadoresRetorno = jogadorServiceMocked.getJogadores();
	
	}
	
	@Given("^vai ser retornado um conjunto com 2 jogadores")
	public void retornoBuscarTodosJogadores() {
	
		assertEquals("Jogador 1", jogadoresRetorno.get(0).getNome());
		assertEquals("Jogador 2", jogadoresRetorno.get(1).getNome());
		assertEquals(2, jogadoresRetorno.size());
		
	}
}
