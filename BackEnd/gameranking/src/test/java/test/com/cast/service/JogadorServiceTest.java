package test.com.cast.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import com.cast.model.Jogador;
import com.cast.service.JogadorService;

public class JogadorServiceTest {

	private JogadorService jogadorService = null;

	@Before
	public void init() {
		jogadorService = Mockito.mock(JogadorService.class);
	}

	@Test
	public void addJogadorInvalido() {

		Jogador jogador = new Jogador("", 0, 0);

		Mockito.doReturn(HttpStatus.BAD_REQUEST).when(jogadorService).addJogador(jogador);
		
		assertEquals(HttpStatus.BAD_REQUEST,jogadorService.addJogador(jogador));
		

	}

	@Test
	public void addJogadorJaCadastrado() {

		Jogador jogadorCadastrado = new Jogador("Teste Cadastrado", 0, 0);

		Mockito.doReturn(HttpStatus.CONFLICT).when(jogadorService).addJogador(jogadorCadastrado);
		assertEquals(HttpStatus.CONFLICT,jogadorService.addJogador(jogadorCadastrado));
	}

	@Test
	public void addVitoria() {

		Jogador jogador = new Jogador("Jogador", 0, 0);
		
		Mockito.doReturn(true).when(jogadorService).addVitoria(jogador);
		assertTrue(jogadorService.addVitoria(jogador));

	}

	@Test
	public void addPartida() {

		Jogador jogador = new Jogador("Jogador", 0, 0);

		Mockito.doReturn(true).when(jogadorService).addPartida(jogador);
		assertTrue(jogadorService.addPartida(jogador));

	}

	@Test
	public void getJogadores() {

		Jogador jogador1 = new Jogador("Jogador 1", 0, 0);
		Jogador jogador2 = new Jogador("Jogador 2", 0, 0);
		List<Jogador> jogadores = new ArrayList<>(Arrays.asList(jogador1, jogador2));

		Mockito.doReturn(jogadores).when(jogadorService).getJogadores();

		List<Jogador> jogadoresRetorno = jogadorService.getJogadores();
		assertEquals("Jogador 1", jogadoresRetorno.get(0).getNome());
		assertEquals("Jogador 2", jogadoresRetorno.get(1).getNome());

	}

}
